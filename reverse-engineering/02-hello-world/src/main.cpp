#include <cstdio>

// Function
void hello_echo() {
    char name[256];

    printf("Hi! What's your name? ");

    scanf("%[^\n]", name);

    printf("Hello, %s!\n", name);
}

int main()
{
    // First function call (no args, no return)
    hello_echo();
}