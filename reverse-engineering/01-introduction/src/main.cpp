#include <cstdio>

// Function with no args, no return
void say_hello()
{
    printf("Hello!\n");
}

// Function with no args
void say_thing(const char* sentence)
{
    printf("Says: %s\n", sentence);
}

// Function with args and return
int sum(int a, int b)
{
    return a + b;
}

// Function with nested calls
int factorial(int k) {
    if (k == 1) return k;
    return factorial(k - 1) * k;
}

int main()
{
    // First function call (no args, no return)
    say_hello();

    // Second function call (no args)
    say_thing("Thing");

    // Third function call (no args, no return)
    printf("Sum: %d\n", sum(5, 3));

    // Forth function call (nested functions)
    printf("Factorial: %d\n", factorial(5));
}